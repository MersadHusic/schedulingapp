package h.mersad.schedulingapp.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Mersad on 5/27/2018.
 */

public class User {

    public int id;

    public static User parseJSON(String response) {
        Gson gson = new GsonBuilder().create();
        return gson.fromJson(response, User.class);
    }
}
