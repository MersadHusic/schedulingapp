package h.mersad.schedulingapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.riontech.calendar.CustomCalendar;
import com.riontech.calendar.dao.EventData;
import com.riontech.calendar.dao.dataAboutDate;

import java.util.ArrayList;
import java.util.Calendar;

import h.mersad.schedulingapp.R;

public class CalendarActivity extends AppCompatActivity {
    private CustomCalendar customCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        customCalendar = findViewById(R.id.customCalendar);

        ArrayList<EventData> eventData = new ArrayList<>();
        EventData eventData1 = new EventData();
        ArrayList<dataAboutDate> dataAboutDates = new ArrayList<>();
        dataAboutDate dataAboutDate = new dataAboutDate();
        dataAboutDate.setSubject("Test");
        dataAboutDate.setRemarks("Test");
        dataAboutDate.setTitle("Test");
        dataAboutDates.add(dataAboutDate);
        eventData1.setData(dataAboutDates);
        eventData.add(eventData1);
        eventData.add(eventData1);
        eventData.add(eventData1);
        customCalendar.addAnEvent("2018-06-03", eventData.size(), eventData);

        FloatingActionButton floatAdd = findViewById(R.id.float_add);
        floatAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Click action
                Intent intent = new Intent(getApplicationContext(), AddMeetingFloatActivity.class);
                startActivity(intent);
            }
        });
    }
}