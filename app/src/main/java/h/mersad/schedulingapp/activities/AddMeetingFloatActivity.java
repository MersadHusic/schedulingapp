package h.mersad.schedulingapp.activities;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import h.mersad.schedulingapp.R;


public class AddMeetingFloatActivity extends AppCompatActivity{

    public EditText inputTitle;
    public TextView inputStart, inputEnd, inputMail, inputTimeAlarm, addAlarm, inputLocation, addMap, inputNotes, statusRepeat, addInvites;
    public Switch daySwitch;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.meeting_activity_add_float);

        inputTitle = findViewById(R.id.input_title);
        inputStart = findViewById(R.id.input_start);
        inputEnd = findViewById(R.id.input_end);
        inputMail = findViewById(R.id.input_mail);
        inputTimeAlarm = findViewById(R.id.input_time_alarm);
        addAlarm = findViewById(R.id.add_alarm);
        inputLocation = findViewById(R.id.input_location);
        addMap = findViewById(R.id.add_map);
        inputNotes = findViewById(R.id.input_notes);
        statusRepeat = findViewById(R.id.status_repeat);
        addInvites = findViewById(R.id.add_invites);
        daySwitch = findViewById(R.id.day_switch);

    }

}









